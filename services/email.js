'use strict'

const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

module.exports = function (fastify, opts, next) {
  fastify.get('/email', function (req, reply) {
    reply.send('this is an example')
  })

  fastify.post('/send', (req, reply) => {
    const msg = {
      to: req.body.destination || process.env.POSTAL_DEST,
      from: req.body.sender || process.env.POSTAL_FROM ,
      subject: req.body.subject || process.env.POSTAL_SUBJECT,
      text: req.body.text || process.env.POSTAL_TEXT,
      html: req.body.text || process.env.POSTAL_TEXT,
    };
    sgMail
    .send(msg)
    .then(() => {
      reply.send("email sent apparently")
    }, error => {
      if (error.response) {
        req.log.error("Error: shit didnt work man");
        reply.send(error.response)
      }
    });
  })
    


  next()
}
